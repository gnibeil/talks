# Talks of S. Liebing 

In this repository, I collect and provide my talks given at serval instances formatted in HTML format.

# Languages 

- HTML 

# WebSlides  
- All presentations presented here using WebSlides. 

# [The starting page](https://gnibeil.gitlab.io/talks/index.html)
- Is a simple html page linking to the individual presentations.

# Python interface for WebSlides 
- [ezprez](https://github.com/QU-UP/ezprez#faq) 
