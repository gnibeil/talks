import numpy as np
from matplotlib.pyplot import * 
import matplotlib.gridspec as gridspec

font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 25}

matplotlib.rc('font', **font)
matplotlib.rc('lines', linewidth=5,markersize=19)

labels = ['LDA','LDA-FLO-SIC','PBE', 'PBE-FLO-SIC', 'SCAN', 'SCAN-FLO-SIC']
width = 0.25

r1 = np.array([212,  86,   2]) / 256
b1 = np.array([  6, 131, 131]) / 256
g1 = np.array([129, 129,   2]) / 256

figure(figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)

ax1 = subplot(gs[0])
ax1.set_ylabel('G2-1: MAE(IP) [eV]')

dx = 0.26

ax1.bar(0,    4.01, width,color=r1)
ax1.bar(dx,   1.99, width,color=r1,hatch='o')
ax1.bar(2*dx, 4.08, width,color=b1)
ax1.bar(3*dx, 1.41, width,color=b1,hatch='o')
ax1.bar(4*dx, 3.71, width,color=g1)
ax1.bar(5*dx, 1.57, width,color=g1,hatch='o')

xtcks = ax1.get_xticks()
myxtcks = [0*dx,1*dx,2*dx,3*dx,4*dx,5*dx]
ax1.set_xticks(myxtcks)
ax1.set_xticklabels(labels)
show()
