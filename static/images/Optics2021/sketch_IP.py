import numpy 
from matplotlib.pyplot import * 

font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 25}

matplotlib.rc('font', **font)
matplotlib.rc('lines', linewidth=5,markersize=19)

def parabola(x,a,b): 
    return (x-a)**2 + b

def line(ax,x1,x2,y,color='gray'):
    ax.plot([x1,x2],[y,y],color=color)

def my_arrow(ax,x,y,dx,dy,color='black',label='arrow'):
    ax.arrow(x,y,dx,dy,zorder=10,width=0.1,color=color,fc=color,ec=color,label=label)

a_shift = -3.5

x1 = numpy.arange(2,10,0.01)
x2 = numpy.arange(8+a_shift,16+a_shift,0.01)


fig1 = figure(1) 
ax = subplot(111) 
ax.plot(x1,parabola(x1,6,2),'-',color='green',label=r'Neutral: $N$')
line(ax,4.6,7.4,4,color='green')
line(ax,3.6,8.4,8,color='green')
line(ax,2.85,9.15,12,color='green')
#line(ax,2.3,9.75,16)
ax.plot(x2,parabola(x2,12+a_shift,18),'-',color='red',label=r'Ion: $N-1$')
line(ax,10+a_shift,14+a_shift,4+18,color='red')
line(ax,9.2+a_shift,14.8+a_shift,8+18,color='red')
line(ax,8.6+a_shift,15.4+a_shift,12+18,color='red')
#arrow(ax,6,4,26)
#arrow(6,4,0,21.5,zorder=10,width=0.1,color='black',fc='black',ec='black')
#arrow(6,4,2,17.5,zorder=10,width=0.1,color='black',fc='black',ec='black')
my_arrow(ax,6,4,0,21.5,color='black',label='vertical ionization')
my_arrow(ax,6,4,2,17.5,color='gray',label='adiabatic ionization')
xlim(0,14)
ylim(0,40)
xticks([])
yticks([])
xlabel('Bond length')
ylabel('Energy')
legend()
show()


